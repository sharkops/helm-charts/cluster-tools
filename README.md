# K8S Cluster Tools

This chart will help you setup a new Kubernetes cluster easily. Everything is optional and can be turned off with `<feature>.enabled: false`.

## Included Features

- MetalLB (L2 LoadBalancer)
- Kong (Ingress Controller / API Gateway)
- external-dns (External DNS updater)
- cert-manager (ACME certificate issuer)

## Creating a new Rancher RKE Cluster
1. When creating a new cluster, select the `External` cloudprovider. This way all deployed nodes will get the required taints.
2. Merge the config below with the generated cluster config `kubelet` configuration (`Edit as YAML (Cluster options)`):
  ```
  kubelet:
    extra_binds:
      - '/csi:/csi:rshared'
      - '/var/lib/csi/sockets/pluginproxy/csi.vsphere.vmware.com:/var/lib/csi/sockets/pluginproxy/csi.vsphere.vmware.com:rshared'
  ```
3. Create you cluster.

Original idea from https://github.com/stefanvangastel/vsphere-cpi-csi-helm and https://github.com/kubernetes-sigs/vsphere-csi-driver 
